using System;
using System.Numerics;

namespace Lab3;

public static class ComplexExtensions
{
    public static double CalculateR(this Complex c) => (1 + Math.Sqrt(1 + 4 * c.Magnitude)) / 2;
    public static double Mod(this Complex z) => Math.Sqrt(z.Real * z.Real + z.Imaginary * z.Imaginary);

}