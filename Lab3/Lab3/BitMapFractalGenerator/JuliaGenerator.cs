﻿using System;
using Avalonia.Media.Imaging;
using System.Numerics;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using Avalonia;
using Avalonia.Media;
using Avalonia.Platform;
using Lab3.CoordinateConverters;

namespace Lab3.BitMapFractalGenerator
{
    internal class JuliaGenerator: IBitMapFractalGenerator
    {
        private const int pixelBytes = 4;
        public readonly JuliaConverter converter;
        public Complex C { get; set; } = new (-0.745, 0.113);
        public Fractal Fractal { get; set; }
        public (int Width, int Height) BitmapSize { get; set; }
        public GridBorder Border => converter.GridBorder;
        public IColorStrategy ColorStrategy { get; set; }

        public JuliaGenerator(Fractal fractal, Size bitmapSize, IColorStrategy colorStrategy)
        {
            Fractal = fractal;
            BitmapSize = ((int)bitmapSize.Width, (int)bitmapSize.Height);
            converter = new JuliaConverter(
                new GridBorder(
                    -1 * (1.0 * bitmapSize.Width / bitmapSize.Height), 
                    1.0 * bitmapSize.Width / bitmapSize.Height, -1, 1
                    ), 
                bitmapSize
                );
            ColorStrategy = colorStrategy;
        }

        public Bitmap GenerateBitMap()
        {
            byte[] bmp = FractalToBitMapBytes();
        
            IntPtr intPtr = Marshal.AllocHGlobal(bmp.Length);
            Marshal.Copy(bmp, 0, intPtr, bmp.Length);
        
            var bitmap = new Bitmap(PixelFormat.Rgba8888, AlphaFormat.Unpremul,
                intPtr, new PixelSize(Convert.ToInt32(BitmapSize.Width), Convert.ToInt32(BitmapSize.Height)), 
                new Avalonia.Vector(96, 96), Convert.ToInt32(BitmapSize.Width) * 4);
    
            Marshal.FreeHGlobal(intPtr);
            return bitmap;
        }

        private byte[] FractalToBitMapBytes()
        {
            int bytes = Convert.ToInt32(BitmapSize.Width * BitmapSize.Height * pixelBytes);
            byte[] rgbValues = new byte[bytes];

            Parallel.For(0, Convert.ToInt32(BitmapSize.Height * BitmapSize.Width), t =>
            {
                int y =  Convert.ToInt32(t / BitmapSize.Width);
                int x =  Convert.ToInt32(t % BitmapSize.Width);

                var point = converter.FromCanvasToGrid((x, y));
                Complex number = new Complex(point.x, point.y);
                Color color = ComplexNumberToColor(number);

                rgbValues[(y * Convert.ToInt32(BitmapSize.Width) + x) * pixelBytes] = color.B;
                rgbValues[(y * Convert.ToInt32(BitmapSize.Width) + x) * pixelBytes + 1] = color.G;
                rgbValues[(y * Convert.ToInt32(BitmapSize.Width) + x) * pixelBytes + 2] = color.R;
                rgbValues[(y * Convert.ToInt32(BitmapSize.Width) + x) * pixelBytes + 3] = 255;
            });

            return rgbValues;
        }

        private  Color ComplexNumberToColor(Complex number)
        {
            int itter = Fractal.CalculateFractalIterationCount(number, C);

            return ItterNumberToColor(itter, number);
        }

        private Color ItterNumberToColor(int itter, Complex number)
        {
            double t = itter * 1.0 / Fractal.MaxIterations;

            return ColorStrategy.ToColor(t,number, C.CalculateR());
        }

    }
}
