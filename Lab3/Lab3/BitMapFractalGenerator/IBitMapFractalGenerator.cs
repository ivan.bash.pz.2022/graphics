using Avalonia.Media.Imaging;

namespace Lab3.BitMapFractalGenerator;

public interface IBitMapFractalGenerator
{
    public Bitmap GenerateBitMap();
    public IColorStrategy ColorStrategy { get; set; }
    public GridBorder Border { get;}
}