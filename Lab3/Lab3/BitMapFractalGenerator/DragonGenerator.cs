using System.Collections.Generic;
using System.Numerics;
using Avalonia;
using Avalonia.Media;
using Avalonia.Media.Imaging;
using Lab3.CoordinateConverters;

namespace Lab3.BitMapFractalGenerator;

public class DragonGenerator: IBitMapFractalGenerator
{
    public readonly DragonConverter Converter;
    public int IterationCount { get; set; } = 10;
    public Size CanvasSize { get; set; }
    public IColorStrategy ColorStrategy { get; set; }
    
    public GridBorder Border => Converter.GridBorder;

    public DragonGenerator(Size canvasSize, IColorStrategy colorStrategy)
    {
        CanvasSize = canvasSize;
        Converter = new DragonConverter(new GridBorder(0, canvasSize.Width, 0, canvasSize.Height), canvasSize);
        ColorStrategy = colorStrategy;
    }
    public Bitmap GenerateBitMap()
    {
        int width = (int)CanvasSize.Width;
        int height = (int)CanvasSize.Height; 
        var bitmap = new RenderTargetBitmap(new PixelSize(width, height));
    
        using(var context = bitmap.CreateDrawingContext()) 
        { 
            Stack<(double, double, double, double, double)> stack = new Stack<(double, double, double, double, double)>();
            stack.Push((width  * 3 / 8.0, height * 3 / 8.0, width  * 5 / 8.0, height * 5 / 8.0, IterationCount));
        
            while (stack.Count > 0)
            {
                var (xa, ya, xc, yc, iter) = stack.Pop();
                if (iter == 0)
                {
                    var startCoord = (xa, ya);
                    var endCoord = (xc, yc);
                    context.DrawLine(
                        new Pen(new SolidColorBrush(ColorStrategy.ToColor( 0.5 ,new Complex(xa,xc), ya/yc))), 
                        Converter.ConvertPoint(new Point(startCoord.xa, startCoord.ya)), 
                        Converter.ConvertPoint(new Point(endCoord.xc, endCoord.yc)));
                }
                else
                {
                    double xb = (xa + xc) / 2 + (yc - ya) / 2;
                    double yb = (ya + yc) / 2 - (xc - xa) / 2;
                    stack.Push((xa, ya, xb, yb, iter - 1));
                    stack.Push((xc, yc, xb, yb, iter - 1));
                }
            }
        }
        return bitmap;
    }

}