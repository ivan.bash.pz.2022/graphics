using System;
using System.IO;
using System.Numerics;
using Avalonia.Controls;
using Avalonia.Controls.Primitives;
using Avalonia.Input;
using Avalonia.Interactivity;
using Avalonia.Media;
using Lab3.BitMapFractalGenerator;
using Lab3.ColorStrategies;
using Size = Avalonia.Size;


namespace Lab3;

public partial class MainWindow : Window
{
    private const double VerticalMove = 0.1;
    private const double HorizontalMove = 0.1;
    private const double ScaleFactor = 1.5;

    private Size size;
    private IBitMapFractalGenerator bitMapGenerator;
    private Fractal fractal;
    public MainWindow()
    {
        InitializeComponent();
    }
    
    private void Window_Loaded(object sender, RoutedEventArgs e)
    {
        size = new Size((int)Canvas.DesiredSize.Width, (int)Canvas.DesiredSize.Height);
        fractal = new Fractal { MaxIterations = Convert.ToInt32(IterationsSlider.Value) };
        GridBorder border = new GridBorder(xMin: -1 * (1.0 * size.Width / size.Height), xMax: (1.0 * size.Width / size.Height), yMin: -1 , yMax: 1);
        bitMapGenerator = new JuliaGenerator(fractal, size, new ColorStrategy1());
        GenerateImage();
    }
    
    private void GenerateImage()
    {
        Image.Width = Canvas.DesiredSize.Width;
        Image.Height = Canvas.DesiredSize.Height;
        Image.Stretch = Stretch.Fill;
        Image.Source = bitMapGenerator.GenerateBitMap();
        Canvas.Focus();
    }
    
    private void MoveInDirectionProportionally(double tx, double ty)
    {
        bitMapGenerator.Border.MoveProportionally(tx,  ty);
        GenerateImage();
    }

    private void Top_OnClick(object? sender, RoutedEventArgs e) => MoveUp();

    private void Bottom_OnClick(object? sender, RoutedEventArgs e) => MoveDown();

    private void Left_OnClick(object? sender, RoutedEventArgs e) => MoveLeft();

    private void Right_OnClick(object? sender, RoutedEventArgs e) => MoveRight();
    
    private void MoveUp() => MoveInDirectionProportionally(0, VerticalMove);
    
    private void MoveDown() => MoveInDirectionProportionally(0, -VerticalMove);
    
    private void MoveRight() => MoveInDirectionProportionally(-HorizontalMove, 0);
    
    private void MoveLeft() => MoveInDirectionProportionally(HorizontalMove, 0);
    private void Scale(double scaleFactor)
    {
        bitMapGenerator.Border.ScaleCenter(scaleFactor);
        GenerateImage();
    }
    
    private void ScaleDown() => Scale(1 / ScaleFactor);

    private void ScaleUp() => Scale(ScaleFactor);
    
    private void Save_OnClick(object? sender, RoutedEventArgs e) => SaveImage();

    private void ZoomIn_OnClick(object? sender, RoutedEventArgs e) => ScaleUp();

    private void ZoomOut_OnClick(object? sender, RoutedEventArgs e) => ScaleDown();

    private void IterationsSlider_OnValueChanged(object? sender, RangeBaseValueChangedEventArgs e)
    {
        if (fractal == null) return;
        if(FractalName.SelectedIndex == 0) 
            fractal.MaxIterations = (int)e.NewValue;
        else
            ((DragonGenerator)bitMapGenerator).IterationCount = (int)e.NewValue > 20 ? 10 : (int)e.NewValue;
        GenerateImage();
    }
    
    private void ColorComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
        if(ColorComboBox == null)
            return;
        IColorStrategy colorStrategy; 
        if (ColorComboBox.SelectedIndex == 0) 
            colorStrategy = new ColorStrategy1();
        else if (ColorComboBox.SelectedIndex == 1) 
            colorStrategy = new ColorStrategy2();
        else 
            colorStrategy = new ColorStrategy3();
            
        bitMapGenerator.ColorStrategy = colorStrategy;
        GenerateImage();
    }

    private void ChangeC_OnClick(object? sender, RoutedEventArgs e)
    {
        if (CRealInput.Text == null || CImagInput.Text == null) return;
        double real;
        double imag;
        try
        {
            real = Convert.ToDouble(CRealInput.Text);
            imag = Convert.ToDouble(CImagInput.Text);
            ((JuliaGenerator)bitMapGenerator).C = new Complex(real, imag);
            GenerateImage();
        }
        catch (Exception exception)
        {
            return;
        }
    }
    
    private async void SaveImage()
    {
        var saveFileDialog = new SaveFileDialog();
        saveFileDialog.Title = "Save Image";
        saveFileDialog.Filters.Add(new FileDialogFilter { Name = "PNG Image", Extensions = { "png" } });
        saveFileDialog.Filters.Add(new FileDialogFilter { Name = "JPEG Image", Extensions = { "jpg", "jpeg" } });

        var result = await saveFileDialog.ShowAsync(this);

        try
        {
            if (!string.IsNullOrWhiteSpace(result))
            {
                using (var fileStream = File.OpenWrite(result))
                {
                    var bitmap = bitMapGenerator.GenerateBitMap();
                    var pngStream = new MemoryStream();
                    bitmap.Save(pngStream, 1);
                    pngStream.Position = 0;
                    pngStream.CopyTo(fileStream);
                }
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
        }
    }
    
    private void Window_KeyDown(object sender, KeyEventArgs e)
    {
        switch (e.Key)
        {
            case Key.S: SaveImage(); break;
            case Key.Up: Canvas.Focus(); MoveUp(); break;
            case Key.Down: Canvas.Focus(); MoveDown(); break;
            case Key.Left: Canvas.Focus(); MoveLeft(); break;
            case Key.Right: Canvas.Focus(); MoveRight(); break;
            case Key.Add: Canvas.Focus(); ScaleUp(); break;
            case Key.Subtract: Canvas.Focus(); ScaleDown(); break;
            case Key.OemPlus: Canvas.Focus(); ScaleUp(); break;
            case Key.OemMinus: Canvas.Focus(); ScaleDown(); break;
        }
    }

    private void FractalName_OnSelectionChanged(object? sender, SelectionChangedEventArgs e)
    {
        if(bitMapGenerator == null) return;

        if (FractalName.SelectedIndex == 0)
        {
            bitMapGenerator = new JuliaGenerator(fractal, size, bitMapGenerator.ColorStrategy);
            IterationsSlider.Maximum = 1000;
            IterationsSlider.Minimum = 100;
        }
        else
        {
            bitMapGenerator = new DragonGenerator(size, bitMapGenerator.ColorStrategy);
            IterationsSlider.Maximum = 20;
            IterationsSlider.Minimum = 1;
            IterationsSlider.Value = 10;
            return;
        }

        GenerateImage();
    }
}