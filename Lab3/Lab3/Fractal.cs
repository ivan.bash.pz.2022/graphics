﻿using System;
using System.Numerics;

namespace Lab3
{
    internal class Fractal
    {
        public int MaxIterations { get; set; }

        public int CalculateFractalIterationCount(Complex point, Complex c)
        {
            int res = 0;
            double r = c.CalculateR();
            for (int i = 0; i < MaxIterations; i++)
            {
                if (r > 0 && point.Mod() > r)
                {
                    break;
                }
                point = point * point + c;
                res++;
            }
            return res;
        }

    }
}
