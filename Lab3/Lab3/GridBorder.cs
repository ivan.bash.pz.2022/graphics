﻿namespace Lab3
{
    public class GridBorder
    {
        public double xMin { get; set; }
        public double xMax { get; set; }
        public double yMin { get; set; }
        public double yMax { get; set; }

        public GridBorder(double xMin, double xMax, double yMin, double yMax)
        {
            this.xMin = xMin;
            this.xMax = xMax;
            this.yMin = yMin;
            this.yMax = yMax;
        }

        public void Move(double x, double y)
        {
            xMin += x;
            xMax += x;
            yMin += y;
            yMax += y;
        }

        public void MoveProportionally(double tx, double ty)
        {
            Move((xMax - xMin) * tx, (yMax - yMin) * ty);
        }

        private void Scale(double x, double y, double scaleFactor)
        {   
            xMin = x - 1 / scaleFactor * (x - xMin);
            xMax = x + 1 / scaleFactor * (xMax - x);
            yMin = y - 1 / scaleFactor * (y - yMin);
            yMax = y + 1 / scaleFactor * (yMax - y);
        }

        public void ScaleCenter(double scaleFactor)
        {
            Scale((xMin + xMax) / 2, (yMin + yMax) / 2, scaleFactor);
        }

    }
}