﻿using System;
using System.Numerics;
using Avalonia.Media;

namespace Lab3.ColorStrategies
{
    public class ColorStrategy2 : IColorStrategy
    {
        
        public Color ToColor(double t,Complex z, double radius)
        {
            if (Math.Abs(t - 1) < 1e-6) return Colors.White;

            return Color.FromArgb(
                255, 
                Convert.ToByte(255 * t), 
                Convert.ToByte(255 * (1 - t)), 
                Convert.ToByte(255 * (z.Mod() / radius > 1 ? 1 : z.Mod() / radius))
                );
        }
    }
}