﻿using System;
using System.Numerics;
using Avalonia.Media;

namespace Lab3.ColorStrategies
{
    public class ColorStrategy3 : IColorStrategy
    {
        public Color ToColor(double t, Complex z, double radius)
        {
            if (Math.Abs(t - 1) < 1e-6) return Colors.Black;

            t = Math.Sin(t * Math.PI * radius / z.Mod());
            byte intensity = (byte)((t + 1) / 2 * 255);
            
            return Color.FromArgb(255 ,intensity, Convert.ToByte(255 - intensity), Convert.ToByte(255 - intensity));
        }
    }
}