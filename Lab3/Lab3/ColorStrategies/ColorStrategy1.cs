﻿using System.Numerics;
using Avalonia.Media;


namespace Lab3.ColorStrategies
{
    public class ColorStrategy1 : IColorStrategy
    {
        public Color ToColor(double t,Complex z, double radius)
        {
            byte r = (byte)(9 * (1 - t) * t * t * t * 255 * z.Mod());
            byte g = (byte)(15 * (1 - t) * (1 - t) * t * t * 255 * radius);
            byte b = (byte)(8.5 * (1 - t) * (1 - t) * (1 - t) * t * 255 * radius / z.Mod());
            return Color.FromArgb(255, r, g, b);
        }
    }
}
