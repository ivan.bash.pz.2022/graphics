﻿using System.Numerics;
using Avalonia;
using Avalonia.Media;

namespace Lab3
{
    public interface IColorStrategy
    {
        public Color ToColor(double t, Complex z, double radius);
    }


}
