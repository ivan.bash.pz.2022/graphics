﻿using Avalonia;

namespace Lab3.CoordinateConverters
{
    public class JuliaConverter
    {  
        private Size canvasSize;

        public GridBorder GridBorder { get; set; }

        public JuliaConverter(GridBorder gridBorder, Size canvasSize)
        {
            this.GridBorder = gridBorder;
            this.canvasSize = canvasSize;
        }

        public (double x, double y) FromCanvasToGrid((double x, double y) p)
        {
            double mappedX = (p.x * (GridBorder.xMax - GridBorder.xMin) / canvasSize.Width) + GridBorder.xMin;
            double mappedY = GridBorder.yMax - (p.y * (GridBorder.yMax - GridBorder.yMin) / canvasSize.Height);
            return (mappedX, mappedY);
        }

        public (double x, double y) FromGridToCanvas((double x, double y) p)
        {
            double mappedX = (p.x - GridBorder.xMin) / (GridBorder.xMax - GridBorder.xMin) * canvasSize.Width;
            double mappedY = (-p.y + GridBorder.yMax) / (GridBorder.yMax - GridBorder.yMin) * canvasSize.Height;
            return (mappedX, mappedY);
        }
    }
}