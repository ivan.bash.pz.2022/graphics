using Avalonia;

namespace Lab3.CoordinateConverters;


public class DragonConverter
{  
    private Size canvasSize;

    public GridBorder GridBorder { get; set; }

    public DragonConverter(GridBorder gridBorder, Size canvasSize)
    {
        GridBorder = gridBorder;
        this.canvasSize = canvasSize;
    }

    public Point ConvertPoint(Point point)
    {
        double x = canvasSize.Width * (point.X - GridBorder.xMin) / (GridBorder.xMax - GridBorder.xMin);
        double y = canvasSize.Height * (1 - (point.Y - GridBorder.yMin) / (GridBorder.yMax - GridBorder.yMin));
        return new Point(x, y);
    }
}    
