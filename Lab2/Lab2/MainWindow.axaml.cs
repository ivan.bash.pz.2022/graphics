using System.Collections.Generic;
using System.Linq;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Controls.Shapes;
using Avalonia.Interactivity;
using Avalonia.Media;
using Avalonia.Layout;

namespace Lab2;

enum MessageType
{
    Error,
    Info
}    


public partial class MainWindow : Window
{
    private Point _startPoint = new Point(450, 300);
    private Dictionary< List<Point>,Polyline> _bezierCurves = new Dictionary< List<Point>,Polyline>();
    private List<Point> _points = new List<Point>();
    private Dictionary<Point,Ellipse>  _ellipses = new Dictionary<Point,Ellipse>();
    private Dictionary<List<Point>, Polyline> _broken = new Dictionary<List<Point>, Polyline>();
    public MainWindow()
    {
        InitializeComponent();
        DrawCoordinateSystem();
    }
    
    private void DrawCoordinateSystem()
    {
        Line xAxis = new Line{ StartPoint = new Point(0, 300), EndPoint = new Point(865, 300), Stroke = Brushes.Black};
        xAxis.StrokeThickness = 2;
        
        Line halfArrow = new Line{ StartPoint = new Point(855, 290), EndPoint = new Point(865, 300), Stroke = Brushes.Black};
        halfArrow.StrokeThickness = 3;
        
        Line anotherHalfArrow = new Line{ StartPoint = new Point(855, 310), EndPoint = new Point(865, 300), Stroke = Brushes.Black};
        anotherHalfArrow.StrokeThickness = 3;
        
        Line yAxis = new Line{ StartPoint = new Point(450, 0), EndPoint = new Point(450, 565), Stroke = Brushes.Black};
        yAxis.StrokeThickness = 2;
        
        Line halfArrowY = new Line{ StartPoint = new Point(440, 555), EndPoint = new Point(450, 565), Stroke = Brushes.Black};
        halfArrowY.StrokeThickness = 3;
        
        Line anotherHalfArrowY = new Line{ StartPoint = new Point(460, 555), EndPoint = new Point(450, 565), Stroke = Brushes.Black};
        anotherHalfArrowY.StrokeThickness = 3;
        
            
        TextBlock xText = new TextBlock 
        { 
            Foreground = Brushes.Black, 
            FontSize = 20, 
            Text = "X", 
            Margin = new Thickness(0, 0, 0, 0), 
            HorizontalAlignment = HorizontalAlignment.Center, 
            VerticalAlignment = VerticalAlignment.Center, 
            RenderTransform = new ScaleTransform{ ScaleY = -1 }
        };
        Canvas.SetLeft(xText, 835);
        Canvas.SetTop(xText, 310);
        
        TextBlock yText = new TextBlock
        {
            Foreground = Brushes.Black,
            FontSize = 20,
            Text = "Y",
            Margin = new Thickness(0, 0, 0, 0),
            HorizontalAlignment = HorizontalAlignment.Center,
            VerticalAlignment = VerticalAlignment.Center,
            RenderTransform = new ScaleTransform{ ScaleY = -1 }
        };
        Canvas.SetLeft(yText, 460);
        Canvas.SetTop(yText, 535);
        
        Canvas.Children.Add(yText);
        Canvas.Children.Add(xText);

        Canvas.Children.Add(yAxis);
        Canvas.Children.Add(xAxis);
        Canvas.Children.Add(halfArrow);
        Canvas.Children.Add(anotherHalfArrow);
        Canvas.Children.Add(halfArrowY);
        Canvas.Children.Add(anotherHalfArrowY);
        
        for (int i = 0; i < 865; i += 20)
        {
            Line xLine = new Line{ StartPoint = new Point(i, 295), EndPoint = new Point(i, 305), Stroke = Brushes.Black};
            xLine.StrokeThickness = 2;
            
            Line yLine = new Line{ StartPoint = new Point(445, i), EndPoint = new Point(455, i), Stroke = Brushes.Black};
            yLine.StrokeThickness = 2;
            
            Canvas.Children.Add(xLine);
            Canvas.Children.Add(yLine);
        }
    }
    
    private void ShowMessage(string message, MessageType type = MessageType.Error)
    {
        var color = type == MessageType.Error ? Brushes.Red : Brushes.Gray;
        var textAlignment = type == MessageType.Error ? TextAlignment.Center : TextAlignment.Left;
        var height = type == MessageType.Error ? 150 : 500;
        var width = type == MessageType.Error ? 450 : 650;
        var errorWindow = new Window
        {
            Height = height,
            Width = width,
            Background = color,
            Content = new TextBlock
            {
                Text = message,
                FontSize = 20,
                TextAlignment = textAlignment,
                Foreground = Brushes.Black,
            },
            Title = type.ToString()
        };
        errorWindow.ShowDialog(this);
    }
    
    private Point ConvertPoint(Point point)
        => new (point.X + _startPoint.X, _startPoint.Y + point.Y);
    
    private Point UnConvertPoint(Point point)
        => new (point.X - _startPoint.X, _startPoint.Y - point.Y);
    
    private void AddPoint_OnClick(object? sender, RoutedEventArgs e)
    {
        var x = X.Text;
        var y = Y.Text;
        if (x == "" || y == "")
        {
            ShowMessage("Введіть координати точки");
            return;
        }
        if (!double.TryParse(x, out var xCoord) || !double.TryParse(y, out var yCoord))
        {
            ShowMessage("Координати повинні бути числами");
            return;
        }
        _points.Add(new Point(xCoord, yCoord));
        Ellipse ellipse = new Ellipse
        {
            Width = 7,
            Height = 7,
            Fill = Brushes.Black
        };
        if(_points.Count > 2)
            _ellipses[_points[^2]].Fill = Brushes.Red;
        _ellipses.Add(_points[^1],ellipse);
        
        Canvas.SetLeft(ellipse, ConvertPoint(_points[^1]).X - 5);
        Canvas.SetTop(ellipse, ConvertPoint(_points[^1]).Y - 5);
        Canvas.Children.Add(ellipse);
    }

    private void DeletePoint_OnClick(object? sender, RoutedEventArgs e)
    {
        if (_points.Count == 0)
        {
            ShowMessage("Немає точок для видалення");
            return;
        }
        Canvas.Children.Remove(_ellipses[_points.Last()]);
        _ellipses.Remove(_points.Last());
        _points.RemoveAt(_points.Count - 1);
        
        if(_points.Count > 1)
            _ellipses[_points[^1]].Fill = Brushes.Black;
    }

    private void AddCurve_OnClick(object? sender, RoutedEventArgs e)
    {
        if (_points.Count < 2)
        {
            ShowMessage("Для побудови кривої Безьє потрібно мінімум 2 точки");
            return;
        }

        List<Point> brokenPoints = new List<Point>();
        BezierCurve bezierCurve = new BezierCurve();
        var curvePoints = bezierCurve.GetCurvePoints(_points, 0.1);
        
        for (int j = 0; j < curvePoints.Count; j++)
            curvePoints[j] = ConvertPoint(curvePoints[j]);
        
        for (int j = 0; j < _points.Count; j++)
            brokenPoints.Add(ConvertPoint(_points[j]));
        Polyline curve = new Polyline
        {
            Stroke = Brushes.Blue,
            StrokeThickness = 2
        };
        Polyline broken = new Polyline
        {
            Stroke = Brushes.Green,
            StrokeThickness = 2
        };
        for (int i = 0; i < brokenPoints.Count; i++)
        {
            broken.Points.Add(brokenPoints[i]);
        }
        
        for (int i = 0; i < curvePoints.Count; i++)
        {
            curve.Points.Add(curvePoints[i]);
        }
        _bezierCurves.Add(_points,curve);
        _broken.Add(_points, broken);
        _points = new List<Point>();
        Canvas.Children.Add(curve);
        Canvas.Children.Add(broken);
    }

    private void DeleteCurve_OnClick(object? sender, RoutedEventArgs e)
    {
        if (_bezierCurves.Count == 0)
        {
            ShowMessage("Немає кривих для видалення");
            return;
        }
        Canvas.Children.Remove(_bezierCurves.Last().Value);
        Canvas.Children.Remove(_broken.Last().Value);

        foreach (var point in _bezierCurves.Last().Key)
        {
            Canvas.Children.Remove(_ellipses[point]);
            _ellipses.Remove(point);
        }
        _bezierCurves.Remove(_bezierCurves.Last().Key);
        _broken.Remove(_broken.Last().Key);
    }

    private void ClearButton_OnClick(object? sender, RoutedEventArgs e)
    {
        foreach (var point in _ellipses.Keys)
        {
            Canvas.Children.Remove(_ellipses[point]);
        }
        foreach (var curve in _bezierCurves.Values)
        {
            Canvas.Children.Remove(curve);
        }
        _ellipses.Clear();
        _bezierCurves.Clear();
        _points.Clear();
        _broken.Clear();
    }

    private void Print_OnClick(object? sender, RoutedEventArgs e)
    {
        if(_bezierCurves.Count == 0)
        {
            ShowMessage("Немає кривих для виведення");
            return;
        }
        var points = new BezierCurve().GetCurvePoints(_bezierCurves.Last().Key, 0.05);
        string result = "";
        for (int i = 0; i < points.Count; i++)
        {
            result += $"Точка {i + 1}: x = {points[i].X.ToString("F2")}, " +
                      $"y = {points[i].Y.ToString("F2")}\n";
        }
        ShowMessage(result, MessageType.Info);
    }

    private void Calculate_OnClick(object? sender, RoutedEventArgs e)
    {
        var i = I.Text;
        if(_bezierCurves.Count == 0)
        {
            ShowMessage("Немає кривих для побудови поліномів Бернштейна");
            return;
        }
        if (!int.TryParse(i, out var iValue))
        {
            ShowMessage("Введіть коректне і");
            return;
        }
        
        BezierCurve bezierCurve = new BezierCurve();
        
        string result = bezierCurve.CalculateBernsteinPolynomials(_bezierCurves.Last().Key, iValue);
        ShowMessage(result, MessageType.Info);
    }
}