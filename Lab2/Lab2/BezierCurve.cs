using System;
using System.Collections.Generic;
using Avalonia;

namespace Lab2;

public class BezierCurve
{
    private List<Point> Points { get; set; } = new List<Point>();

    public List<Point> GetCurvePoints(List<Point> points, double step)
    {
        Points = points;
        var curvePoints = new List<Point>();

        for (double t = 0; t <= 1; t += step)
        {
            curvePoints.Add(GetPointAt(t));
        }

        return curvePoints;
    }

    private Point GetPointAt(double t)
    {
        var n = Points.Count - 1;
        var binomialCoefficients = new List<double>();

        for (int i = 0; i <= n; i++)
        {
            binomialCoefficients.Add(BinomialCoefficient(n, i));
        }

        double x = 0;
        double y = 0;

        for (int i = 0; i <= n; i++)
        {
            var p = Points[i];
            var b = binomialCoefficients[i] * Math.Pow(1 - t, n - i) * Math.Pow(t, i);
            x += p.X * b;
            y += p.Y * b;
        }

        return new Point(x, y);
    }

    private static double BinomialCoefficient(int n, int k)
    {
        if (k > n / 2)
        {
            k = n - k;
        }

        double c = 1;
        for (int i = 1; i <= k; i++)
        {
            c *= (n - k + i) / (double) i;
        }

        return c;
    }
    
    public string CalculateBernsteinPolynomials(List<Point> points, int i)
    {
        var n = i;
        var result = "";
        for (double k = 0; k <= 1; k += 0.05)
        {
            Point point = new Point();
            result += $"P({k.ToString("F2")}) = ";
            for (int j = 0; j <= n; j++)
            {
                double Pj = BinomialCoefficient(n, j) * Math.Pow(k, j) * Math.Pow(1 - k, n - j);
                result += $"{Pj.ToString("F2")} * P_{j}";
                point = new Point(point.X + Pj * points[j].X, point.Y + Pj * points[j].Y);
                if (j != n)
                    result += " + ";
                else
                    result += $" = ({point.X.ToString("F2")}, {point.Y.ToString("F2")})\n";
            }
            
            point = new Point();
            result += "P(1) = ";
            for (int j = 0; j <= n; j++)
            {
                double Pj = BinomialCoefficient(n, j) * Math.Pow(k, j) * Math.Pow(1 - 1, n - j);
                result += $"{Pj.ToString("F2")} * P_{j}";
                point = new Point(point.X + Pj * points[j].X, point.Y + Pj * points[j].Y);
                if (j != n)
                    result += " + ";
                else
                    result += $" = ({point.X.ToString("F2")}, {point.Y.ToString("F2")})\n";
            }
        }

        return result;
    }
}
